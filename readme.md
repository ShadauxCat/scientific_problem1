This is my answer to the scientific games programming challenge problem #1.

**Requirement: You must have numpy installed to run the answer. If you don't already have it, you can install it with `pip install numpy`. Additionally, to create the graph output, you must also install plotly via `pip install plotly`, though the script will run without this - it just won't generate graphs.**

There are two scripts included in this: one to create test data, another to process it.

The first, create_data.py, is executed as follows:

```
usage: create_data.py [-h] [--output OUTPUT] [--count COUNT]
                      [--average-age AVERAGE_AGE]

Randomly generate data to be read by answer.py

optional arguments:
  -h, --help            show this help message and exit
  --output OUTPUT, -o OUTPUT
                        Location of the .csv file containing the data
  --count COUNT, -c COUNT
                        Number of birth/death pairs to generate
  --average-age AVERAGE_AGE, -a AVERAGE_AGE
                        Average age of people in range, within approximated
                        normal distribution
```

This script will create random data in csv format detailing birth and death dates.

The second script processes the data and is executed as follows:

```
usage: answer.py [-h] csv_file

Read the given csv file containing comma-separated births and deaths, then
find the year(s) with the most living people

positional arguments:
  csv_file    Location of the .csv file containing the data

optional arguments:
  -h, --help  show this help message and exit
```

Example execution:

```
$ python answer.py data.csv
Most people alive: 751046 in year 1950
```

In addition to the text output, answer.py also creates two html files: graph.html and table.html

The first of these, graph.html, shows the number of living people per year as a bar graph.

The second, as might be surmised, shows them as a table.

As per my comments at the top of answer.py (see them for more detail), 
there are a number of performance techniques I could implement in C++ 
to improve this, but I chose to implement this in python instead of C++
to avoid the challenges inherent in C++ when I'm uncertain what the
development environment (operating system, installed libraries, architecture,
CPU instruction set, etc) that this code would be tested in.