# Answer to Scientific Games coding challenge problem 1, implemented in python
# I chose to implement this in Python, even though C++ could have many many performance advantages,
# because Python is easier to distribute. Not knowing the system configuration this would be running on,
# C++ would have been a bit of a hassle; however, with C++, the following benefits could have been drawn:
#
# 1) Improved memory usage and performance by referencing the data in the file directly by pointer, rather
#    than doing string copies that Python requires
# 2) Improved performance via careful memory management - this could be done (within the given constraints)
#    with no dynamic memory allocation at all. Python allocates for every variable.
# 3) Use of SIMD instructions when available, since the same operation is being performed on multiple variables
#    simultaneously
#
# Additionally, performance could be improved via multithreading, giving each thread a (carefully calculated)
# chunk of the input file and having them all compute individual outputs, then combining them, though that effort
# seems beyond the scope of this challenge, and would be less useful in python due to the global import lock,
# mandating that python use multiprocessing to maximize this benefit.
#
# Finally, performance could be improved, particularly in C++, by using a binary data set and treating the data
# explicitly as an array of integers via reinterpret_cast, but for this exercise I wanted to use a human-readable
# data set so that my generated data could be more easily reviewed.
#
# I have left comments throughout the file in specific places where C++ could have improved performance.

from __future__ import print_function
import argparse
# Using numpy to get a bit of perf improvement out of it, since this is crunching lots of numbers.
import numpy


# This may be dealing with a potentially humongous data set.
# (The default generated is 1,000,000 lines, but let's pretend it can be infinite)
# Using a generator here so that I can avoid having all of it in memory at once if it is truly massive.
# f.readlines() reads it all at once into a list.
# ...Although since this is python, if it's massive enough to cause memory problems, it will take ages to parse anyway.
def ReadLines(f):
	"""
	Read the lines of a file one by one as a generator, rather than returning a list.

	:param f: Open file
	:type f: file
	:return: String containing one line of the file
	:rtype: str
	"""
	val = f.readline()
	while val != "":
		# Strip the trailing newline
		# PERF: In C++, this could return a direct pointer reference into the file.
		# In Python, this is doing not one, but two string copies. Ew.
		yield val[:-1]
		val = f.readline()


def Parse(filename):
	"""
	Parse the given input file, counting the number of people alive each year.
	Then print the result and generate graph.html and table.html files to show the counts.

	:param filename: File to process
	:type filename: str
	"""

	# This array will contain all the collected data on number of people alive per year
	# It's offset by 1900 since that's the data constraint that was provided.
	# Using numpy to get some perf improvement.
	data = numpy.zeros(101)

	with open(filename, "r") as f:
		# Iterate through the lines of the file, collecting data
		for line in ReadLines(f):
			# Could use csv module here, but since each line contains exactly two values, partition is simpler.
			birth, _, death = line.partition(",")

			# If the format's wrong, I'll let int() do error checking and throw an exception for us.
			birth = int(birth)
			death = int(death)
			assert(death >= birth)

			# Finally increment the value for every year this person was alive.
			# The second part of this slice is death-1899, because that person is being counted as having been alive
			# on their death year, and the second parameter is exclusive rather than inclusive.
			# PERF: This could be done very nicely in C++ using SIMD instructions, but in Python this will suffice.
			data[birth-1900:death-1899] += 1

	# Using numpy.argmax() does prevent us from finding multiple years that had the same value.
	# I'm accepting that as the behavior in this case since that situation was unspecified in the documentation
	# It could be made to find and report ties with a bit more work, but would lose some of numpy's speed
	# Though this one case of searching through a 100-value array wouldn't suffer that much from that change.
	# Probably not even noticeably.
	maxidx = numpy.argmax(data)

	print("Most people alive: {} in year {}".format(int(data[maxidx]), maxidx + 1900))
	try:
		# Generating the graphs requires plotly to be installed.
		# Plotly can be installed with `pip install plotly`
		# If plotly is missing, though, don't crash on an ImportError; I'll print my own error and exit cleanly.
		import plotly
		import plotly.graph_objs as go
		from plotly.tools import FigureFactory as FF

		graph = go.Bar(
			x=list(range(1900,2001)),
			y=list(data)
		)
		plotly.offline.plot([graph], filename='graph.html', auto_open=False)
		table = FF.create_table([["Year", "Living People"]] + [[key+1900, value] for key,value in enumerate(data)])
		plotly.offline.plot(table, filename='table.html', auto_open=False)

	except ImportError:
		print("Could not generate graphs. Please install plotly via 'pip install plotly'")


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Read the given csv file containing comma-separated births and deaths,"
												 " then find the year(s) with the most living people")
	parser.add_argument("csv_file", help="Location of the .csv file containing the data")
	args = parser.parse_args()

	Parse(args.csv_file)
