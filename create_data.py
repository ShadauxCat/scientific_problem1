from __future__ import print_function
import random
import argparse


def CreateCSV(filename, count, averageAge):
	"""
	Create a csv file with randomly generated birth and death years between 1900 and 2000
	:param filename: Output file
	:type filename: str
	:param count: Number of birth/death years to generate
	:type count: int
	:param low: Lowest birth/death year to generate
	:type low: int
	:param high: highest birth/death year to generate
	:type high: int
	"""
	with open(filename, "w") as f:
		for _ in range(count):
			maxAge = 100
			# Approximate a normal/gaussian distribution by rolling two numbers and adding together
			# This is only an approximation of normal, but it suits our needs because it constrains it within
			# a minimum and maximum value.
			# Although it's only really approximately gaussian if the average age is half the max age
			# It will cluster around the average age, which is the part we really want, so we can simulate
			# actual human lifespans.
			ageLeft =random.randint(0, averageAge)
			ageRight = random.randint(0, maxAge-averageAge)
			age = ageLeft + ageRight

			birth = random.randint(1900, 2000-age)
			death = birth + age
			f.write("{},{}\n".format(birth, death))

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Randomly generate data to be read by answer.py")
	parser.add_argument("--output", "-o", default="data.csv", help="Location of the .csv file containing the data")
	parser.add_argument("--count", "-c", default=1000000, help="Number of birth/death pairs to generate")
	parser.add_argument("--average-age", "-a", default=70,
						help="Average age of people in range, within approximated normal distribution")
	args = parser.parse_args()

	CreateCSV(args.output, args.count, args.average_age)
